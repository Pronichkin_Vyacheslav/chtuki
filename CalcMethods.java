/**
* <h1> Класс, реализующий некоторый функционал </h1>
* <h1> стандартного класса Math. </h1>
*
* <h3> Содержит следующие методы: </h3>
* <li> addition - сложение </li>
* <li> subtraction - вычитание </li>
* <li> multiplication - умножение </li>
* <li> division - целочисленное деление </li>
* <li> division - вещественное деление </li>
* <li> mod - остаток от деления </li>
* <li> pow - возведение в указанную степень </li>
*
* @author Pronichkin Vyacheslav 16IT18k
*
*/

public class CalcMethods{

        public static void notificationTwick(boolean twick){
		    if(twick){
			    System.out.println("Вы ввели вещественные числа");
		    } else {
			    System.out.println("Вы ввели целые числа");
		    }
			
		/** 
		 * Метод для сложения двух чисел.
         *
         * Оповещает пользователя о том, какого типа данные он ввёл: вещественные или целые.
	     *
	     */
			
		}
		
		
        public static void notification(){
		    System.out.println("Введите выражение для вычисления. Выражение должно состоять из двух чисел и одного знака операции по примеру '5 + 5' ");
        }
		
    /** 
	 * Метод для оповещения пользователя
	 * 
	 * Оповещает пользователя о том, что нужно ввести выражение, подходящее под образец для калькулятор.
	 *
	 */
	

        public static void addition(int x, int y){
            int result = x + y;

            System.out.println("Первое слагаемое: " + x + ", второе слагаемое : " + y + ", сумма = " + result);

	/** 
	 * Метод для сложения двух чисел.
     *
	 * x - Первое слагаемое
	 * y - Второе слагаемое
	 *
	 * result - Результат сложения
	 *
	 */

        }

        public static void subtraction(int x, int y) {
            int result = x - y;

            System.out.println("Уменьшаемое: " + x + ", вычитаемое: " + y + ", разность = " + result);

    /**
     * Метод для вычитания двух чисел.
     *
	 * x - Уменьшаемое
	 * y - Вычитаемое
	 *
	 * result - Результат вычитания
	 *
	 */

        }

        public static void multiplication(int x, int y) {
            int result = x * y;

            System.out.println("Множимое: " + x + ", множитель: " + y + ", произведение = " + result);

	/**
	 * Метод для умножения двух чисел.
     *
	 * x - Множимое
	 * y - Множитель
	 *
	 * result - Результат произведения
	 *
	 */

        }

        public static void division(int x, int y) {
            int result = x / y;

            System.out.println("Делимое: " + x + ", делитель: " + y + ", частность = " + result);

	/** 
     * Метод для деления двух чисел.
	 * 
	 * x - Делимое
	 * y - Делитель
	 *
	 * result - Результат деления
	 *
	 */
	 
		}

	    public static void division(double x, double y) {
            double result = x / y;

            System.out.println("Делимое: " + x + ", делитель: " + y + ", частность = " + result);

	/**
     * Метод для деления двух вещественных чисел.
	 *
	 * x - Делимое
	 * y - Делитель
	 *
	 * result - Результат деления
	 *
	 */
	 
        }

        public static void mod(int x, int y) {
            int result = x % y;

            System.out.println("Делимое: " + x + ", делитель: " + y + ", остаток = " + result);

	/** 
     * Метод для нахождения остатка от деления.
	 *
	 * x - Делимое
	 * y - Делитель
	 *
	 * result - Остаток
	 *
	 */

        }

        public static void pow(int x, int y) {
            int result = 0;
            switch(y){
                case 0:
                    result = 1;
                    break;
                case 1:
                    result = x;
                    break;
                default:
                    for (int i = 0; i < y; i++) {
                        result = x * x;
                    }
            }

            System.out.println("Число, возводимое в степень: " + x + ", степень: " + y + ", результат возведения = " + result);
	/**
     * Метод возведения в степень.
     *
	 * x - Число, возводимое в степень
	 * y - Сама степень
	 *
	 * result - Результат возведения
	 *
	 * Если значение степени равно нулю, то результат становится равным одному.
	 * Если значение степени равно одному, то результат становится равным числу, заданному в переменной x.
	 *
	 */

        }

    }