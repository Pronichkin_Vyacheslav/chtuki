/**
* Моя версия калькулятора.
*
* Вычисляет значение введенного в строчку выражения, но пока допустимы только выражение из 2х
* операндов.
*
* @author Pronichkin Vyacheslav 16IT18k
*
*/

import java.util.Scanner; //Сканнер, метод мейн и всё такое, в общем: классика жанра.

public class Calculator {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
	CalcMethods.notification(); //Вызов метода для приветствия пользователя.
    String expression = scanner.nextLine(); //Введение следующей строки с клавиатуры пользователем.
    boolean twick = expression.contains("."); //Выявление того, являются ли введёные данные вещественными или целочисленными путём поиска точки.
    String arrSym[] = expression.split(" "); //Разделение строки на массив из трёх символов, разделителем является пробел.
    CalcMethods.notificationTwick(twick); //Вызов метода для оповещения пользователя о введённом типе данных.
	double x = Double.parseDouble(arrSym[0]); //Обьявление переменной и присваивание значения из массива, преобразование символа из типа String в тип Double.
	double y = Double.parseDouble(arrSym[2]); //То же самое, что и в строке выше.
        
    switch(arrSym[1]){ //Оператор switch для выявления нужного действия из списка путём распознания знака операции.
        case "+": //Сложение.
            CalcMethods.addition((int)x, (int)y); //Вызов метода, явное преобразование в Integer для того, чтобы не было всяких некрасивых точек или других штук.
            break;
        case "-": //Вычитание.
            CalcMethods.subtraction((int)x, (int)y); 
            break;
        case "*": //Произведение.
            CalcMethods.multiplication((int)x, (int)y);
            break;
        case "/": //Деление.
            if(twick){ //Оператор if для выявления, целочисленное ли деление или вещественное.
				CalcMethods.division(x, y); //В этом случае деление вещественное с данными типа Double.
            } else {
				CalcMethods.division((int)x, (int)y); //В этом случае деление целочисленное с данными типа Integer.
            }
            break;
        case "%": //Нахождение остатка.
            CalcMethods.mod((int)x, (int)y);
            break;
        case "^": //Возведение в степень.
            CalcMethods.pow((int)x, (int)y);
            break;
		default: 
            System.out.println("Знак операции не найден, вычисление невозможно."); //В любом другом случае, если знак операции не найден.
            break;
        }
        
    }
}
